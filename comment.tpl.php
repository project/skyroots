<?php
?> 
<div id="commentTop"></div>
<div id="commentLevel1">
    <div <?php print $attributes; ?> 
    <?php if ($new != ''): ?>
      <span class="new"><?php print $new; ?></span>
    <?php endif; ?>
      <h3 class="title"><?php print $title; ?></h3>
    <?php if ($picture) print $picture; ?>
      <span class="comment-submitted"><?php print t('Submitted on ') . format_date($comment->timestamp, 'custom', 'F jS, Y') . t(' by '); ?> <?php print theme('username', $comment); ?></span>
      <div class="content">
    <?php print $content; ?>
      </div>
    <?php if($signature): ?>
      <div class="signature">
        <?php print $signature; ?>
        
      </div>
    <?php endif; ?>
      <div class="links">
    <?php print $links; ?>
    
      </div>
    </div>
</div>
<div id="commentBottom"></div>
