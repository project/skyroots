<?php

drupal_rebuild_theme_registry();

/**
  This function gets used quite a bit in this theme. It basically cleans 
  things up so that I can use various terms in class and ids.
  */

function hunchbaque_id_safe($string) {
  if (is_numeric($string{0})) {
    // if the first character is numeric, add 'n' in front
    $string = 'n'. $string;
  }
  return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/**
  Support for IE specific stylesheets. Just use the 'ie' (ie. $output['ie']) 
  array for all versions of Internet Explorer of the version number (ie. 
  $output['6']). Support for lt, lte, gt and gte will come at a later date. 
  */

function hunchbaque_ie() {
  $output['ie'] = array(
    'ie.css',
  );
  
  return $output;
}

function hunchbaque_ie_styles() {
  $ie_css = hunchbaque_ie();
  $output = "\n";
  foreach ($ie_css as $version => $value) {
    if($version == 'ie'){
      $output .= '<!--[if IE]>'."\n";
    } else {
      $output .= '<!--[if IE '.$version.']>'."\n";
    }
    foreach ($value as $stylesheet) {
      $output .= '  <style type="text/css" media="screen, projection">@import "'.base_path().path_to_theme().'/'.$stylesheet.'";</style>'."\n";
    }
    $output .= '<![endif]-->'."\n";
  }
  print $output;
}

/**
  I need some new variables for pages. The following function allows me to
  do that. I do rebuild all teh body classes and ids since I use a 
  non-standard region setup and I like to add a few of my own.
  */

function hunchbaque_preprocess_page($vars) {
  // Determine if the page is the front page and apply pertinent classes 
  // if it is. Otherwise use that arg variables to construct the class and
  // id names.
  switch (TRUE) {
    case ($vars['is_front']):
      $body_id = 'id="front-page"';
      $body_class[] = 'front';
      break;

    case (!$vars['is_front']):
      $body_class[] = 'not-front';
      break;
  }
  
  switch (TRUE) {
    case (!arg(0)):
      $body_id = 'id="error-page"';
      $body_class[] = 'is-error';
      break;

    case (!$vars['is_front']):
      $path_alias = drupal_get_path_alias(arg(0).'/'.arg(1));
      $body_id = 'id="'.hunchbaque_id_safe($path_alias).'-page"';
      $path_explode = explode('/', $path_alias);
      $body_class[] = $path_explode[0].'-section';
      break;
  }
  
  // Check the logged in state, and add the appropriate class if so.
  if ($vars['logged_in']) {
    $body_class[] = 'logged-in';
  }
  
  // If we are looking at a full node view, construct a class to specify 
  // the node type.
  if (isset($vars['node'])) {
    $body_class[] = 'ntype-'.hunchbaque_id_safe($vars['node']->type);
  }
  
  // If editting a node, add a special class just for that.
  if (arg(2) == 'edit') {
    $body_class[] = 'edit';
  }
  
  // Normally Drupal can give me all the classes I need for the body, but 
  // beings I use a non-standard regions setup, we have to make our own.
  switch (TRUE) {
    case ($vars['main_supplements'] && $vars['secondary_supplements']):
      $body_class[] = 'sidebars';
      break;
    
    case ($vars['main_supplements']):
      $body_class[] = 'main-sidebar';
      break;
      
    case ($vars['secondary_supplements']):
      $body_class[] = 'secondary-sidebar';
      break;
    
    default:
      $body_class[] = 'no-sidebars';
      break;
  }
  
  // Now we take all those classes and ids that were created for the body 
  // and compile them into a single variable.
  $vars['body_attributes'] = $body_id.' class="'.implode(' ', $body_class).'"';
}

/**
  The following function compiles classes and ids for the individual nodes
  and then loaded them into a $attributes variable for the template.
  */

function hunchbaque_preprocess_node($vars) {
  $node_id = 'node-'.$vars['type'].'-'.$vars['nid'];
  $node_class[] = 'node';
  $node_class[] = 'ntype-'.$vars['type'];
  if ($vars['teaser']) {
    $node_class[] = 'teaser';
  }
  if (!$vars['status']) {
    $node_class[] = 'not-published';
  }
  if ($vars['promote']) {
    $node_class[] = 'promoted-to-front';
  }
  if ($vars['sticky']) {
    $node_class[] = 'sticky';
  }
  $node_class[] = $vars['zebra'];
  
  $vars['attributes'] = 'id="'.$node_id.'" class="'.implode(' ', $node_class).'"';
}

/**
  Comments need proper ids and classes too. This is pretty much the same 
  process as the page and nodes.
  */

function hunchbaque_preprocess_comment($vars) {
  $comment_id = 'node-'.$vars['node']->nid.'-comment-'.$vars['comment']->cid;
  $comment_class[] = 'comment';
  if ($vars['comment']->uid == $vars['node']->uid) {
    $comment_class[] = 'author';
  }
  if ($vars['comment']->status) {
    $comment_class[] = 'not-published';
  }
  if ($vars['comment']->new) {
    $comment_class[] = 'new';
  }
  $comment_class[] = $vars['zebra'];
  
  $vars['attributes'] = 'id="'.$comment_id.'" class="'.implode(' ', $comment_class).'"';  
}

/**
  Set the block classes.
  */

function hunchbaque_preprocess_block($vars) {
  $block_id = 'block-'.$vars['block']->module.'-'.$vars['block']->bid;
  $block_class[] = 'block';
  $block_class[] = 'btype-'.$vars['block']->module;
  $block_class[] = $vars['zebra'];
  
  $vars['attributes'] = 'id="'.$block_id.'" class="'.implode(' ', $block_class).'"';  
}