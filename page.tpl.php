<?php

/*
This theme is based extensively on the css Zen Garden submission 123 - 'Skyroots'
by Axel Hebenstreit, http://www.sonnenvogel.com/ 
 
The current incarnation was ported over by Ekendra Dasa - http://www.gopala.org/ - please make him feel loved by dropping a line on his blog. He spent his entire Sunday fiddling with the CSS for you.
 
 Photoshop files are zipped in the 'images' folder so you can customize.
 
 Please leave this message intact!
 
 ::: go-run-gaa-!! :::
 
 */

 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?>
<style type="text/css" media="all">
@import "<?php print base_path() . path_to_theme() . "/style.css" ?>";
</style>
<?php print $scripts; ?>
</head>
<body <?php print $body_attributes; ?>>
<div id="page" class="clear-block">
  <div id="header">
    <?php if ($site_name): ?>
    <h1><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a></h1>
    <?php endif; ?>
    <?php if ($site_slogan): ?>
    <span id="tagline"><?php print $site_slogan; ?></span>
    <?php endif; ?>
  </div>
  <!-- /#header -->
  <?php if ($header || $breadcrumb): ?>
  <div id="breadcrumb">
    <?php if ($breadcrumb): ?>
    <?php print $breadcrumb; ?>
    <?php endif; ?>
  </div>
  <!-- /#breadcrumb -->
  <?php endif; ?>
  <div id="left-margin">
    <?php if ($primary_links): ?>
    <div id="main_menu_top"> </div>
    <div id="main_menu"> <?php print theme('links', $primary_links, array('class' => 'main-links')) ?> </div>
    <!-- /#main_menu -->
    <?php if ($secondary_links): ?>
    <div id="secondary_menu"> <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?> </div>
    <!-- /#secondary_menu -->
    <?php endif; ?>
    <?php endif; ?>
    <!-- this displays the blocks -->
    <?php if ($left): ?>
    <div id="blocktop"> </div>
    <div id="block"> <?php print $left; ?> </div>
    <?php endif; ?>
  </div>
  <div id="wrapper">
    <div id="subwrapper">
      <div id="container">
        <div id="content">
          <?php if ($help): ?>
          <?php print $help; ?>
          <?php endif; ?>
          <?php if ($messages): ?>
          <?php print $messages; ?>
          <?php endif; ?>
          <?php if ($mission): ?>
          <div id="mission"><?php print $mission; ?></div>
          <?php endif; ?>
          <?php if ($content_top):?>
          <div id="content-top"> <?php print $content_top; ?> </div>
          <?php endif; ?>
          <?php if ($title): ?>
          <h2 class="title"><?php print $title; ?></h2>
          <?php endif; ?>
          <?php if ($tabs): ?>
          <div class="tabs"> <?php print $tabs; ?> </div>
          <?php endif; ?>
          <?php if ($content): ?>
          <?php print $content; ?>
          <?php endif; ?>
        </div>
      </div>
      <!-- /#container -->
      <?php if ($main_supplements): ?>
      <div id="main-supplements"> <?php print $main_supplements; ?> </div>
      <!-- /#main-supplements -->
      <?php endif; ?>
    </div>
    <!-- /#subwrapper -->
    <?php if ($secondary_supplements): ?>
    <div id="secondary-supplements"> <?php print $secondary_supplements; ?> </div>
    <!-- /#secondary-supplements -->
    <?php endif; ?>
    
      <?php if ($footer || $footer_message): ?>
  		<div id="footer">
    		<?php if ($footer): ?>
    			<?php print $footer; ?>
    		<?php endif; ?>
    		<?php if ($footer_message): ?>
    			<?php print $footer_message; ?>
    		<?php endif; ?>
  		</div>  <!-- /#footer -->
  <?php endif; ?>
  <?php if ($closure): ?>
  <?php print $closure; ?>
  <?php endif; ?>



  </div> <!-- /#wrapper -->
</div> <!-- /#page -->
</body>
</html>
